<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace HyperfTest\Cases;

use HyperfTest\HttpTestCase;
use App\Model\User;
use App\Model\Recommendation;
use Hyperf\Testing\Client;
/**
 * @internal
 * @coversNothing
 */
class ExampleTest extends HttpTestCase
{

    public function testRec()
    {
      $client = make(Client::class);
      $res = $client->get('/',['source'=>'USD','target'=>'JPY','amount'=> '$1,525']);
      $this->assertSame($res['status'], 'success');

      $res = $client->get('/',['source'=>'TWD','target'=>'JPY','amount'=> '$1,525']);
      $this->assertSame($res['status'], 'success');

      $res = $client->get('/',['source'=>'JPY','target'=>'USD','amount'=> '$100,000']);
      print_r($res);
      $this->assertSame($res['status'], 'success');

      $res = $client->get('/',['source'=>'PHP','target'=>'JPY','amount'=> '$1,525']);
      $this->assertNotSame($res['status'], 'success');
    }

}
