FROM hyperf/hyperf:8.0-alpine-v3.16-swoole
WORKDIR /var/www
COPY . /var/www
#RUN composer install --no-dev -o && php bin/hyperf.php

EXPOSE 9501

ENTRYPOINT ["php", "/var/www/bin/hyperf.php", "start"]
