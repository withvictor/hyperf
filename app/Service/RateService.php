<?php

declare(strict_types=1);

namespace App\Service;
class RateService 
{
  public $CUR_FAIL = 'currency fail';
  public $INT_FAIL = 'int fail';
  public $SUCCESS = 'success';

  public $CURIES = 'currencies';

  public $CURS = ['TWD','JPY','USD'];


  public $RATES = '{
                  "currencies": {
                      "TWD": {
                      "TWD": 1,
                      "JPY": 3.669,
                      "USD": 0.03281
                    },
                      "JPY": {
                      "TWD": 0.26956,
                      "JPY": 1,
                      "USD": 0.00885
                    },
                      "USD": {
                      "TWD": 30.444,
                      "JPY": 111.801,
                      "USD": 1
                    }
                  }
              }';

  /*
   * 匯率轉換
   */
  public function transfer($amount, string $source , string $target)
  {
    if(!in_array($source, $this->CURS) || !in_array($target, $this->CURS)){
      return $this->states($this->CUR_FAIL);
    } 
    $amount = $this->intTransfer($amount);

    if(!is_numeric($amount)){
      return $this->states($this->INT_FAIL);
    } 

    $rate = json_decode($this->RATES ,true); 
    $amount = number_format($rate[$this->CURIES][$source][$target] * $amount); 

    return $this->states($this->SUCCESS,$this->amountSuccess( (float)$amount) );
  }
  /*
   * 金額顯示 
   */
  public function amountSuccess($amount){
    $formattedNumber = number_format($amount, 2, '.', ',');
    return  '$' . $formattedNumber; 
  }
   
  /*
   * 狀態顯示 
   */
  public function states(string $status , $amount=0){
    return ['status'=>$status,'amount'=>$amount];
  }

  /*
   * 數字轉換 
   */
  public function intTransfer($stringAmount){
    $cleanedAmount = str_replace(['$',','], '', $stringAmount);
    $amount = intval($cleanedAmount);
    return $amount;
  }
}
