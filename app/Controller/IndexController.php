<?php
declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;
use Elasticsearch\ClientBuilder;
use Hyperf\Guzzle\RingPHP\PoolHandler;
use Swoole\Coroutine;
use App\Service\RateService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\Metric\Contract\MetricFactoryInterface;
class IndexController extends AbstractController
{

    /**
     * @var RateService
     */
    #[Inject]
    private $rateService;

    public function index()
    {
        
        $amount = $this->request->input('amount', 0);
        $source = $this->request->input('source', 0);
        $target = $this->request->input('target', 0);

        $result = $this->rateService->transfer($amount ,$source , $target);

        return [
            'status' => $result['status'] ,
            'amount' => "{$result['amount']}",
        ];
    }
}
